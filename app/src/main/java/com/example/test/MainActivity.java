package com.example.test;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import com.bumptech.glide.Glide;


public class MainActivity extends AppCompatActivity {

    ImageView image,image1,image2,image3;
    HTTPHandler handler;
    String web_result;
    JSONArray array;
    JSONObject object;
    View.OnClickListener onImage,onImage1,onImage2,onImage3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = findViewById(R.id.image);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        //  handler = new HTTPHandler();

        onImage= new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getMessages();
                Toast.makeText(getApplicationContext(), "Miss queen" , Toast.LENGTH_SHORT).show();
            }
        };
        image.setOnClickListener(onImage);

        onImage1= new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //getMessages();
                Toast.makeText(getApplicationContext(), "Android" , Toast.LENGTH_SHORT).show();
            }
        };
        image1.setOnClickListener(onImage1);

        onImage2= new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getMessages();
                Toast.makeText(getApplicationContext(), "UX-UI design" , Toast.LENGTH_SHORT).show();
            }
        };
        image2.setOnClickListener(onImage2);
        onImage3= new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getMessages();
                Toast.makeText(getApplicationContext(), "Hello world" , Toast.LENGTH_SHORT).show();
            }
        };
        image3.setOnClickListener(onImage3);
        getMessages();
    }

    public void getMessages () {

        //     web_result = "[{title: 'Мясо', price: 20, image: 'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg'}, {title: 'Фри', price: 10, image2: 'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg'}, {title: 'Суп', price: 10, image3: 'https://i.pinimg.com/736x/eb/cc/79/ebcc79e2985d8a679f56f30aa8a568c1.jpg'}, {title: 'Цезарь', price: 15, image4: 'https://tri-gusya.ru/wp-content/uploads/2021/11/salat-czezar-405x330.jpg'}]";
        //  web_result="[{id:1,image:'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg',title:'один'},{id:2,image:'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg',title:'два'},{id:3,image:'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg',title:'три'},{id:4,image:'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg',title:'четыри}]";
        // try {
        //  array = new JSONArray(web_result);
        //  object = array.getJSONObject(0);
        Glide.with(this)
                .load("https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg")
                .into(image);
        //    Glide.with(this).load("https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg").into(image);

//                object = array.getJSONObject(1);
        Glide.with(this)
                .load("https://1.bp.blogspot.com/-N8K-ofVV96E/XYxTMnVWXqI/AAAAAAAAATk/QxeBWGQR8VQW8_XfnCkeEo_DNvPLKncbACLcBGAsYHQ/s640/ad_logo_twitter_card.png")
                .into(image1);
//
//                object = array.getJSONObject(2);
        Glide.with(this)
                .load("https://pbs.twimg.com/media/C0MhSyjW8AAMCcy.jpg")
                .into(image2);
//
//                object = array.getJSONObject(3);
        Glide.with(this)
                .load("https://i.ytimg.com/vi/B6jCFtcqZzA/hqdefault.jpg")
                .into(image3);

//            } catch (JSONException e) {
//                throw new RuntimeException(e);
//            }
//

    }
//    public  void onClick(String text){
//        Toast.makeText(this, text,Toast.LENGTH_LONG).show();
//
//    }




}